/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import formbank from './src/screen/formbank';
AppRegistry.registerComponent(appName, () => App);
