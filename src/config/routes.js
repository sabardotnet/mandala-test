import React from 'react';
import formbank from '../formbank';
import forminsert from '../forminsert';

​
const routes = {
    formbank : { screen: formbank },
    forminsert: { screen: forminsert }
}
​
export default routes;