import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    ScrollView,
    SafeAreaView,
    StatusBar,
    FlatList
} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
let DATA = [
    { id: '1', title: 'Indra Pratama (BCA)', bank: '0123456789' },
    { id: '2', title: 'Indra Pratama (Permata)', bank: '0123456789' },
    { id: '3', title: 'Indra Pratama (Prima)', bank: '0123456789' },
];
// for($i=0;$i<100;$i++){
//     DATA.push({
//         id : 'key-'+$i,
//         title : 'Data ke '+$i,
//         bank : 'BCA'
//     })
// }

function Item({ title, bank }) {
    return (
        <View style={styles.container_norek}>
            <Icon style={{ paddingLeft: 20 }} name="credit-card" size={25} />
            <View style={{ flexDirection: "column", marginLeft: 10 }}>
                <Text style={{ fontWeight: 'bold' }}>{title}</Text>
                <Text style={{ color: 'gray' }}>Norek : {bank}</Text>
            </View>
        </View>
    )
}
export default class forminsert extends Component {
    state = { language: '' };
    render() {
        return (

            <View style={styles.container}>
                <View style={styles.text_header} >
                    <View style={styles.container_header}>
                        <View style={styles.header_back}>
                            <Icon name="arrow-left" size={15}/>
                        </View>
                        <View style={styles.header_text}>
                            <Text style={styles.header_text1}>Cairkan Komisi</Text>
                        </View>
                        <View style={styles.header_back}>

                        </View>
                    </View>
                </View>
                <View style={styles.amount} >
                    <TextInput placeholder="Masukkan Jumlah Amount"
                    />
                </View>
                <View style={styles.tambah_rekening} >
                    <Text style={{ marginBottom: 10, fontSize: 20, fontWeight: 'bold' }}>
                        Transfer Ke
                </Text>
                    <View style={styles.parent}>
                        <TouchableOpacity style={styles.con_btn_tambah_rekening} onPress={() => this.props.navigation.navigate("About")}>
                            <View style={styles.btn_tambah_rekening}>
                                <Icon style={{ paddingLeft: 20 }} name="plus" />
                                <Text style={{ paddingLeft: 10 }}>Tambah Rekening</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.list_rekening} >
                    {/* <ScrollView style={{backgroundColor : '#fff'}}> */}
                    <FlatList
                        data={DATA}
                        renderItem={({ item }) => <Item title={item.title} bank={item.bank} />}
                        keyExtractor={item => item.id}
                    />
                    {/* </ScrollView> */}
                </View>
                <View style={styles.tambol_lanjut} >
                    <TouchableOpacity style={styles.btn_lanjut}  onPress={() => this.props.navigation.navigate("About")} >
                        <View style={styles.header_back}>
                        </View>
                        <View style={styles.header_text}>
                            <Text style={styles.header_text2}>Lanjutkan</Text>
                        </View>
                        <View style={styles.header_back}>
                            <Icon name="arrow-right" color="#fff" />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: "white"
    },
    text_header: {
        flex: 1,
        backgroundColor: "white",
    },
    container_header: {
        flex: 1,
        flexDirection: "row",
        backgroundColor: "white",
        marginTop: 20,
        marginBottom: 5,
    },
    header_back: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        // backgroundColor : "#95a5a6"
    },
    header_text: {
        flex: 4,
        justifyContent: "center",
        alignItems: "center"
    },
    header_text1: {
        fontSize: 20,
        fontWeight: 'bold',
        alignItems: "center",
        justifyContent: "center"
    },
    amount: {
        // flex : 1,
        padding: 10,
        // backgroundColor : "#3498db"
        borderBottomColor: '#f2f2f2',
        borderBottomWidth: 1,
    },
    tambah_rekening: {
        flex: 1,
        padding: 10,
        marginBottom: 20,
    },
    parent: {
    },
    con_btn_tambah_rekening: {
        backgroundColor: 'white',
        paddingVertical: 10,
        borderRadius: 10,
        shadowColor: 'black',
        shadowOpacity: 0.9,
        elevation: 10,
    },
    btn_tambah_rekening: {
        // flex : 1, 
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-start",
        height: 40,
    },
    list_rekening: {
        flex: 6,
        padding: 10
    },
    container_norek: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        borderBottomColor: '#f2f2f2',
        borderBottomWidth: 1,
        paddingTop: 10,
        paddingBottom: 10
        // justifyContent : "center"
    },
    tambol_lanjut: {
        flex: 1,
        paddingHorizontal: 10,
        marginBottom: 10,
    },
    btn_lanjut: {
        flex: 1,
        flexDirection: "row",
        backgroundColor: '#90aafb',
        paddingVertical: 10,
        borderRadius: 10,
        shadowColor: 'black',
        shadowOpacity: 0.9,
        elevation: 10,
    },
    header_text2: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
    }
})
