import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Picker,
    SafeAreaView,
    StatusBar,
    FlatList
} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
export default class forminsert extends Component {
    state = { language: '' };
    render() {
    return (

        <View style={styles.container}>
            <View style={styles.text_header} >
                <View style={styles.container_header}>
                    <TouchableOpacity style={styles.header_back} onPress={() => this.props.navigation.navigate("Home")}>
                        <View  >
                                <Icon name="arrow-left" size={15} />
                        </View>
                    </TouchableOpacity>

                    <View style={styles.header_text}>
                        <Text style={styles.header_text1}>Rekening Bank</Text>
                    </View>
                    <View style={styles.header_back}>

                    </View>
                </View>
            </View>
            <View style={styles.amount} >
                <View style={styles.input_form}>
                    <TextInput placeholder="No. Rekening" />
                </View>
                <View style={styles.input_form}>
                    <TextInput placeholder="Rek A/N" />
                </View>
                <View style={styles.input_form}>
                    <Picker
                        selectedValue={this.state.language}
                            onValueChange={(itemValue, itemIndex) =>
                            this.setState({language: itemValue})
                        }>
                        <Picker.Item label="BCA" value="bca" />
                        <Picker.Item label="Mandiri" value="mandiri" />
                        <Picker.Item label="Permata" value="permata" />
                        <Picker.Item label="CIMB NIAGA" value="cimb" />
                        <Picker.Item label="BRI" value="bri" />
                    </Picker>
                </View>                                
            </View>
            <View style={styles.tambol_lanjut} >
                <TouchableOpacity style={styles.btn_lanjut} >
                    <View style={styles.header_back}>
                    </View>
                    <View style={styles.header_text}>
                        <Text style={styles.header_text2}>Lanjutkan</Text>
                    </View>
                    <View style={styles.header_back}>
                        <Icon name="arrow-right" color="#fff" size={15}/>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: "white"
    },
    text_header: {
        flex: 1,
        backgroundColor: "white",
    },
    container_header: {
        flex: 1,
        flexDirection: "row",
        backgroundColor: "white",
        marginTop: 20,
        marginBottom: 5,
    },
    header_back: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        // backgroundColor : "#95a5a6"
    },
    header_text: {
        flex: 4,
        justifyContent: "center",
        alignItems: "center"
    },
    header_text1: {
        fontSize: 20,
        fontWeight: 'bold',
        alignItems: "center",
        justifyContent: "center"
    },
    amount: {
        flex: 8,
        padding: 10,
        // backgroundColor : "#3498db"

    },
    input_form : {
        borderBottomColor: '#f2f2f2',
        borderBottomWidth: 1,
        marginBottom : 5,
    },
    tambah_rekening: {
        flex: 1,
        padding: 10,
        marginBottom: 20,
    },
    parent: {
    },
    con_btn_tambah_rekening: {
        backgroundColor: 'white',
        paddingVertical: 10,
        borderRadius: 10,
        shadowColor: 'black',
        shadowOpacity: 0.9,
        elevation: 10,
    },
    btn_tambah_rekening: {
        // flex : 1, 
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-start",
        height: 40,
    },
    list_rekening: {
        flex: 6,
        padding: 10
    },
    container_norek: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        borderBottomColor: '#f2f2f2',
        borderBottomWidth: 1,
        paddingTop: 10,
        paddingBottom: 10
        // justifyContent : "center"
    },
    tambol_lanjut: {
        flex: 1,
        paddingHorizontal: 10,
        marginBottom: 10,
    },
    btn_lanjut: {
        flex: 1,
        flexDirection: "row",
        backgroundColor: '#90aafb',
        paddingVertical: 10,
        borderRadius: 10,
        shadowColor: 'black',
        shadowOpacity: 0.9,
        elevation: 10,
    },
    header_text2: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
    }
})
